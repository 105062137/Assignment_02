# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

    這次的lab我認為最重要的就是state的控制,如果能寫完主架構,其他細部的function就可以查資料來實作,state的設計要是出問題,後面會比較麻煩,而這次小朋友下樓梯我用了
    
兩個簡單的state來實作,一個是開始的主選單,另外是遊戲主體,透過enter的偵測來轉換state,也另外用status來確定遊戲是否結束

    另外這次作業較困難的地方是素材的蒐集,讓我深深感受到美術對於遊戲的重要性,碰到釘子的叫聲是自己錄的,延遲大概1秒左右,其他部份幾乎全部都可以從講義上找到解決方法
    
    特殊功能包括帶刺的地板,彈簧地板,滾輪地板

    排行榜因為技術問題所以砍掉了,其他基本功能都有做